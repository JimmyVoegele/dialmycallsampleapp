﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DialMyCalls._Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="color:#0091ba">Dial My Calls Sample Application</h2>
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="3" Skin="Office2007">
        <Tabs>
            <telerik:RadTab Text="Contacts" Width="225px" Selected="true"></telerik:RadTab>
            <telerik:RadTab Text="Text Messages" Width="225px"></telerik:RadTab>
            <telerik:RadTab Text="Calls" Width="225px" Selected="True"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0" CssClass="outerMultiPage">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            
            <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" AllowPaging="True" CellSpacing="0"
                GridLines="None" OnNeedDataSource="RadGrid1_NeedDataSource" PageSize="10">
                <MasterTableView AutoGenerateColumns="true">
                </MasterTableView>
            </telerik:RadGrid>
            <br />
            <table>
                <tr>
                    <td>
                        <telerik:RadLabel ID="lblFirstName" runat="server" Text="First Name:" />
                    </td>
                    <td>
                        <telerik:RadTextBox ID="txtFirstName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadLabel ID="lblLastName" runat="server" Text="Last Name:" />
                    </td>
                    <td>
                        <telerik:RadTextBox ID="txtLastName" runat="server" />
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadLabel ID="lblPhoneNumber" runat="server" Text="Phone Number:" />
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtPhoneNumber" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <telerik:RadLabel ID="lblEmail" runat="server" Text="Email:" />
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtEmail" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <telerik:RadLabel ID="lblGroups" runat="server" Text="Group:" />
                        </td>
                        <td>
                            <telerik:RadComboBox ID="cmbGroups" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadButton ID="btnCreateNewContact" runat="server" Text="Create Contact" OnClick="btnCreateNewContact_Click"/>
                        </td>
                    </tr>
                </table>
            <telerik:RadLabel ID="lblReturnMessge" runat="server" Text="" ForeColor="Red" />
            <br />
            <br />
            <telerik:RadLabel ID="lblCreateGroup" runat="server" Text="Group Name:" />
            <telerik:RadTextBox ID="txtCreateGroupName" runat="server" />
            <telerik:RadButton ID="btnCreateGroup" runat="server" Text="Create Group" OnClick="btnCreateGroup_Click"/>
            <br />
            <br />
            <telerik:RadLabel ID="lblDeleteContacts" runat="server" Text="Contacts:" />
            <telerik:RadComboBox ID ="cmbDeleteContacts" runat="server" Width="1000px"></telerik:RadComboBox>
            <telerik:RadButton ID="btnDeleteContacts" runat="server" Text="Delete Contact" OnClick="btnDeleteContact_Click"/>
            <br />
            <br />

        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="RadPageView2">
            <br />
            <telerik:RadLabel ID="lblBroadcastName" runat="server" Text="Broadcast Name:" />
            <telerik:RadTextBox ID="txtBroadcastName" runat="server" Text="Broadcast Name"/>
            <br />
            <br />
            <telerik:RadLabel ID="lblMessage" runat="server" Text="Message Text:" />
            <telerik:RadTextBox ID="txtMessage" runat="server" Text="This is the text of the message." Width="500px"/>
            <br />
            <br />
            <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid2" runat="server" AllowPaging="True" CellSpacing="0" GridLines="None" OnNeedDataSource="RadGrid2_NeedDataSource" PageSize="10" >
                <MasterTableView AutoGenerateColumns="false">
                    <Columns>
                        <telerik:GridTemplateColumn > 
                                <ItemTemplate> 
                                    <asp:CheckBox ID="CheckBox1" runat="server"/>             
                                </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                        <telerik:GridBoundColumn DataField = "FirstName" HeaderText ="First Name"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField = "LastName" HeaderText ="Last Name"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField = "Phone" HeaderText ="Phone"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField = "Email" HeaderText ="Email"></telerik:GridBoundColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <%--<telerik:RadButton ID="btnGetPreRecordedCalls" runat="server" Text="Get Calls" OnClick="btnGetPreRecordedCalls_Click"/>--%>
            <%--<telerik:RadButton ID="btnGetPreRecordedText" runat="server" Text="Get Text" OnClick="btnGetPreRecordedText_Click"/>--%>
            <telerik:RadButton ID="btnSendText" runat="server" Text="Send text Message" OnClick="btnSendText_Click"/>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView3">
            <br />
            <telerik:RadLabel ID="lblCallBroadcastName" runat="server" Text="Call Name:" />
            <telerik:RadTextBox ID="txtCallBroadcastName" runat="server" Text="My call name"/>
            <br />
            <br />
            <telerik:RadLabel ID="lblRecordings" runat="server" Text="Recordings:" />
            <telerik:RadComboBox ID ="cmbRecordings" runat="server" Width="1000px"></telerik:RadComboBox>
            <br />
            <br />
            <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid3" runat="server" AllowPaging="True" CellSpacing="0" GridLines="None" OnNeedDataSource="RadGrid3_NeedDataSource" PageSize="10" >
                <MasterTableView AutoGenerateColumns="false">
                    <Columns>
                        <telerik:GridTemplateColumn > 
                                <ItemTemplate> 
                                    <asp:CheckBox ID="CheckBox1" runat="server"/>             
                                </ItemTemplate> 
                        </telerik:GridTemplateColumn> 
                        <telerik:GridBoundColumn DataField = "FirstName" HeaderText ="First Name"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField = "LastName" HeaderText ="Last Name"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField = "Phone" HeaderText ="Phone"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField = "Email" HeaderText ="Email"></telerik:GridBoundColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <%--<telerik:RadButton ID="btnGetPreRecordedCalls" runat="server" Text="Get Calls" OnClick="btnGetPreRecordedCalls_Click"/>--%>
            <%--<telerik:RadButton ID="btnGetPreRecordedText" runat="server" Text="Get Text" OnClick="btnGetPreRecordedText_Click"/>--%>
            <telerik:RadButton ID="btnMakeCall" runat="server" Text="Make Call" OnClick="btnMakeCall_Click"/>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
