﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

using Telerik.Web.UI;
using IO.DialMyCalls.Model;

namespace DialMyCalls
{
    public partial class _Default : Page
    {
        DialMyCalls dmc = new DialMyCalls();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCreateContact_Click(object sender, EventArgs e)
        {


        }

        protected void btnCreateNewContact_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text.Length == 0)
            {
                lblReturnMessge.Text = "You must enter a First Name.";
                return;
            }

            if (txtLastName.Text.Length == 0)
            {
                lblReturnMessge.Text = "You must enter a Last Name.";
                return;
            }

            string strPhoneNumber = txtPhoneNumber.Text.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
            if (strPhoneNumber.Length == 0)
            {
                lblReturnMessge.Text = "You must enter a Phone Number.";
                return;
            }
            else
            {


                if (strPhoneNumber.Length != 10)
                {
                    lblReturnMessge.Text = "You have entered an invalid phone number.";
                    return;
                }
                long nPhoneNumber = 0;

                try
                {
                    nPhoneNumber = Convert.ToInt64(strPhoneNumber);
                }
                catch
                {
                    lblReturnMessge.Text = "There was an error converting your phone number to 10 digits.";
                    return;
                }

                if (txtEmail.Text.Length == 0)
                {
                    lblReturnMessge.Text = "You must enter an Email Address.";
                    return;
                }
                else
                {
                    if (!txtEmail.Text.Contains("@"))
                    {
                        lblReturnMessge.Text = "You must enter a valid Email Address.";
                        return;
                    }
                    if (!txtEmail.Text.Contains("."))
                    {
                        lblReturnMessge.Text = "You must enter a valid Email Address.";
                        return;
                    }

                }

                List<string> lst = new List<string>();
                lst.Add(cmbGroups.SelectedItem.Value.Trim());

                lblReturnMessge.Text = dmc.CreateContact(txtFirstName.Text, txtLastName.Text, strPhoneNumber, "", txtEmail.Text, lst);
                RadGrid1.Rebind();
            }
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataTable dt = dmc.GetContacts();
            RadGrid1.DataSource = dt;

            cmbDeleteContacts.Items.Clear();

            foreach (DataRow item in dt.Rows)
            {
                string strName = item["FirstName"].ToString().Trim() + " " +  item["LastName"].ToString().Trim() + " " + item["Phone"].ToString().Trim();

                string strPhone = item["Guid"].ToString().Trim() ;

                RadComboBoxItem rcbItem = new RadComboBoxItem(strName, strPhone);
                cmbDeleteContacts.Items.Add(rcbItem);
            }

            Dictionary<string, string> Groups = dmc.GetGroups();
            cmbGroups.Items.Clear();
            foreach (KeyValuePair<string, string> result in Groups)
            {
                RadComboBoxItem itemGRoup = new RadComboBoxItem(result.Value, result.Key);
                cmbGroups.Items.Add(itemGRoup);
            }
        }

        protected void RadGrid2_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGrid2.DataSource = dmc.GetContacts();
        }

        protected void RadGrid3_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            Dictionary<string,string> Recordings = dmc.GetRecordings();

            foreach (KeyValuePair<string, string> result in Recordings)
            {
                RadComboBoxItem item = new RadComboBoxItem(result.Value, result.Key);
                cmbRecordings.Items.Add(item);
            }

            RadGrid3.DataSource = dmc.GetContacts();
        }

        protected void btnSendText_Click(object sender, EventArgs e)
        {
            List<ContactAttributes> lstCA = new List<ContactAttributes>();

            GridTableView GTV = RadGrid2.MasterTableView;


            foreach (GridDataItem item in GTV.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("CheckBox1");
                if (chk.Checked)
                {
                    var FirstName = item["Firstname"].Text.Trim();
                    var LastName = item["Lastname"].Text.Trim();
                    var Phone = item["Phone"].Text.Trim();
                    var Email = item["Email"].Text.Trim();
                    ContactAttributes CA = new ContactAttributes(Phone, FirstName, LastName, Email);
                    lstCA.Add(CA);
                }

            }

            dmc.SendText(txtBroadcastName.Text, txtMessage.Text, lstCA);
        }



        protected void btnMakeCall_Click(object sender, EventArgs e)
        {
            Guid CallerID = dmc.GetCallerID();

            RadComboBoxItem cmbItem = cmbRecordings.SelectedItem; 
            Guid RecordingID = new Guid(cmbItem.Value);

            List<ContactAttributes> lstCA = new List<ContactAttributes>();

            GridTableView GTV = RadGrid3.MasterTableView;


            foreach (GridDataItem item in GTV.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("CheckBox1");
                if (chk.Checked)
                {
                    var FirstName = item["Firstname"].Text.Trim();
                    var LastName = item["Lastname"].Text.Trim();
                    var Phone = item["Phone"].Text.Trim();
                    var Email = item["Email"].Text.Trim();
                    ContactAttributes CA = new ContactAttributes(Phone, FirstName, LastName, Email);
                    lstCA.Add(CA);
                }

            }

            dmc.MakeCall(txtCallBroadcastName.Text, CallerID, RecordingID, lstCA);
        }

        protected void btnCreateGroup_Click(object sender, EventArgs e)
        {
            dmc.CreateGroup(txtCreateGroupName.Text);
            Dictionary<string, string> Groups = dmc.GetGroups();
            cmbGroups.Items.Clear();
            foreach (KeyValuePair<string, string> result in Groups)
            {
                RadComboBoxItem itemGRoup = new RadComboBoxItem(result.Value, result.Key);
                cmbGroups.Items.Add(itemGRoup);
            }
        }

        protected void btnDeleteContact_Click(object sender, EventArgs e)
        {
            dmc.DeleteContact(cmbDeleteContacts.SelectedItem.Value.Trim());
            RadGrid1.Rebind();
        }
    }
}