﻿using System;
using System.Data;
using System.Collections.Generic;

using IO.DialMyCalls.Api;
using IO.DialMyCalls.Client;
using IO.DialMyCalls.Model;
using System.Diagnostics;


namespace DialMyCalls
{
    public class DialMyCalls
    {

        public DialMyCalls()
        {
            try
            {
                Configuration.Default.ApiKey.Add("X-Auth-ApiKey", "cb18bd24cc1429fd97c4bfda94429321");
            }
            catch (Exception ex)
            {   
                Debug.Print("Already Loaded" + ex.Message);
            }
        }

        public DataTable GetContacts()
        {
            var apiInstance = new Contacts();


            var Contacts = apiInstance.GetContacts();

            var str = Contacts.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

            string strFirstName = string.Empty;
            string strLastName = string.Empty;
            string strPhoneNumber = string.Empty;
            string strEmail = string.Empty;
            string strGuid = string.Empty;

            DataTable DT = new DataTable();

            DT.Columns.Add("FirstName");
            DT.Columns.Add("Lastname");
            DT.Columns.Add("Phone");
            DT.Columns.Add("Email");
            DT.Columns.Add("GUID");


            foreach (string strElement in ContactString)
            {
                if (strElement.ToLower().Contains("\"id"))
                {
                    strGuid = strElement.Substring(10);
                }

                if (strElement.ToLower().Contains("\"firstname"))
                {
                    strFirstName = strElement.Substring(17);
                }
                if (strElement.ToLower().Contains("\"lastname"))
                {
                    strLastName = strElement.Substring(17);
                }

                if (strElement.ToLower().Contains("\"phone"))
                {
                    strPhoneNumber = strElement.Substring(15);
                }

                if (strElement.ToLower().Contains("\"email"))
                {
                    strEmail = strElement.Substring(15);
                    DT.Rows.Add(CleanElement(strFirstName), CleanElement(strLastName), CleanElement(strPhoneNumber), CleanElement(strEmail), CleanElement(strGuid));
                }
                
            }

            return DT;
        }

        private string CleanElement(string strContact)
        {
            string strCleanContact = string.Empty;

            strCleanContact = strContact.Replace("\"firstname\"", "").Replace("\"lastname\"", "").Replace("\"phone\"", "").Replace("\"email\"", "").Replace(".", "").Replace(":", "").Replace("\"", "").Replace(",", "");

            return strCleanContact;
        }

        public Guid GetCallerID()
        {
            var apiInstanceCallerIDs = new CallerIds();

            var CallerIDs = apiInstanceCallerIDs.GetCallerIds();

            string ID = CallerIDs.ToString();

            Guid g = new Guid("bf27d82c-7d08-11e7-ba1d-0cc47a8124e8");

            return g;
        }



        public void CreateGroup(string strGroupName)
        {
            var apiInstance = new Groups();

            var createGroupParameters = new CreateGroupParameters(strGroupName);

            apiInstance.CreateGroup(createGroupParameters);

        }

        public Dictionary<string, string> GetRecordings()
        {
            Dictionary<string, string> dct = new Dictionary<string, string>();

            var apiInstanceRecordings = new Recordings();
            var Recordings = apiInstanceRecordings.GetRecordings();

            var str = Recordings.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in ContactString)
            {
                if (strElement.ToLower().Contains("id"))
                {
                    strID = strElement.Substring(10);
                }
                if (strElement.ToLower().Contains("name"))
                {
                    strName = strElement.Substring(13); ;
                    dct.Add(CleanElement(strID.Trim()), CleanElement(strName.Trim()));
                }
            }

            return dct;
        }

        public Dictionary<string, string> GetGroups()
        {
            Dictionary<string, string> dct = new Dictionary<string, string>();

            var apiInstance = new Groups();
            var Groups = apiInstance.GetGroups();

            var str = Groups.ToString();

            string[] stringSeparators = new string[] { "\r\n" };

            var ContactString = str.Split(stringSeparators, StringSplitOptions.None);

            string strID = string.Empty;
            string strName = string.Empty;



            foreach (string strElement in ContactString)
            {
                if (strElement.ToLower().Contains("id"))
                {
                    strID = strElement.Substring(10);
                }
                if (strElement.ToLower().Contains("name"))
                {
                    strName = strElement.Substring(13); ;
                    strID = CleanElement(strID.Trim());
                    strName = CleanElement(strName.Trim());
                    dct.Add(strID.Trim(), strName.Trim());
                }
            }

            return dct;
        }
        public string CreateContact(string strFirstName, string strLastName, string strPhoneNumber, string strExtension, string strEmail)
        {
            return CreateContact(strFirstName, strLastName, strPhoneNumber, strExtension, strEmail, null);
        }

        public string CreateContact(string strFirstName, string strLastName, string strPhoneNumber, string strExtension, string strEmail, List<string> strGroup)
        {
            var apiInstance = new Contacts();
            var createContactParameters = new CreateContactParameters(strFirstName, strLastName, strPhoneNumber, strExtension, strEmail, null, strGroup); // CreateContactParameters | Request body

            try
            {
                // Add Contact
                Object result = apiInstance.CreateContact(createContactParameters);
                Debug.WriteLine(result);
            }
            catch (Exception ex)
            {
                Debug.Print("Exception when calling Contacts.CreateContact: " + ex.Message);
                if (ex.Message.Contains("This phone number already exists as a contact."))
                {
                    //lblReturnMessge.Text = "A contact with that phone number already exists";
                    return "This phone number already exists as a contact.";
                }
                else
                {
                    //lblReturnMessge.Text = "Contact Creation Failed :" + ex.Message;
                    return ex.Message;
                }

            }

            return "Contact created successfully.";
        }

        public string DeleteContact(string strContactID)
        {
            var apiInstance = new Contacts();

            try
            {
                // Add Contact
                Object result = apiInstance.DeleteContactById(strContactID);
            }
            catch (Exception ex)
            {
                    return ex.Message;
            }

            return "Contact deleted successfully.";

        }

        public void SendText(string strBroadcastName, string strMessage, List<ContactAttributes> lstCA)
        {
            var apiInstance = new Texts();

            Guid g = new Guid("9935ac66-7d08-11e7-b57a-0cc47a8124e8");
            List<string> lst = new List<string>();
            lst.Add(strMessage);

            var createTextParameters = new CreateTextParameters(strBroadcastName, g, lst, null, true, false, null, null, null, lstCA); // CreateTextParameters | Request body

            try
            {
                // Create Text
                Object result = apiInstance.CreateText(createTextParameters);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Texts.CreateText: " + e.Message);
            }
        }

        public void MakeCall(string strBroadcastName, Guid CallerID, Guid RecordingID, List<ContactAttributes> lstCA)
        {
            var apiInstance = new Calls();
            var apiInstanceCallerID = new CallerIds();


            var apiInstanceList = apiInstanceCallerID.GetCallerIds();

            var createCallParameters = new CreateCallParameters(strBroadcastName, CallerID, RecordingID, null, null, true, true, true, null, lstCA); // CreateCallParameters | Request body

            try
            {
                // Create Call
                Object result = apiInstance.CreateCall(createCallParameters);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling Calls.CreateCall: " + e.Message);
            }

        }

    }
}